﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Obstacle : MonoBehaviour
{

    private GameObject player; 
    private Text placarDisplay;
    private float placar;

    void Start(){

        player = GameObject.FindGameObjectWithTag("Player");
        placarDisplay = GameObject.FindGameObjectWithTag("Placar").GetComponent<UnityEngine.UI.Text>();
    }

    private void OnTriggerEnter2D(Collider2D collision){

        if(collision.tag == "Border"){ 
            if(GameObject.FindGameObjectWithTag("Player") != null){
                placar = float.Parse(placarDisplay.text) + 1;
                placarDisplay.text = placar.ToString("R");
            }           
            
            Destroy(this.gameObject);
        }

        else if(collision.tag == "Player"){
            Destroy(player.gameObject);
        }
    }
}
