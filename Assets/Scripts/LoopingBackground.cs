﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoopingBackground : MonoBehaviour
{

    public float velocidade;
    public Renderer renderer;

    void Update()
    {
        renderer.material.mainTextureOffset += new Vector2(velocidade * Time.deltaTime, 0f);
    }
}
