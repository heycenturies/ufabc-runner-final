﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float velocidade;
    private Rigidbody2D rb;
    private Vector2 direcao;

    // Pega a property de rigidBody do objecr pra a gente poder manipular nas outras funcoes do script
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        float eixoY = Input.GetAxisRaw("Vertical");
        direcao = new Vector2(0,eixoY).normalized;
    }

    // vai ser chamado sempre que acontecer algo com a fisica do componente associado ao script, entao eh muito melhor que usar o Update()
    void FixedUpdate(){
        rb.velocity = new Vector2(0,direcao.y * velocidade);
    }
}
